import QtQuick 2.12
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami

Kirigami.ApplicationWindow {
    visible: true
    title: qsTr("RSS Reader")

    pageStack.initialPage: [_mainPage]

    Kirigami.Page
    {
        id: _mainPage
        mainAction: Action {
            text: qsTr("Add")
            icon.name: "kt-add-feeds"
            onTriggered: _newSourceDialog.open()
        }

        leftAction: Action {
            text: qsTr("Search")
            icon.name: "edit-find"
        }

        rightAction: Action {
            text: qsTr("Mark")
            icon.name: "love"
            checkable: true
            checked: false
        }


        Kirigami.CardsGridView {
            anchors.fill: parent
            model: ListModel {
                ListElement {title: "hello this is a news title"; body: "short bofy of the new story"}
                ListElement {title: "2hello this is a news title"; body: "short bofy of the new story"}
                ListElement {title: "3hello this is a news title"; body: "short bofy of the new story"}
                ListElement {title: "4hello this is a news title"; body: "short bofy of the new story"}
                ListElement {title: "5hello this is a news title"; body: "short bofy of the new story"}
            }

            delegate: Kirigami.Card {
                height: 200
                width: 200

                banner.source: "qrc:/example.png"
                banner.title: model.title
            }
        }
    }

    globalDrawer: Kirigami.GlobalDrawer {

        bannerImageSource: "qrc:/banner.jpeg"

        actions: [
            Kirigami.Action
            {
                text: "All Feeds"
                icon.name: "feed-subscribe"
            },

            Kirigami.Action
            {
                text: "Categories"
                icon.name: "view-pim-news"
            },

            Kirigami.Action
            {
                text: "Favorite"
                icon.name: "love"
            },

            Kirigami.Action
            {
                text: "Recent"
                icon.name: "folder-open-recent"
            },

            Kirigami.Action
            {
                text: "Mix"
                icon.name: "view-refresh"
            },

            Kirigami.Action {
                text: "archive"
                icon.name: "archive-extract"
            },

            Kirigami.Separator
            {

            },

            Kirigami.Action
            {
                text: "Settings"
                icon.name: "settings-configure"
            }
        ]
    }


    NewSourceForm
    {
        id: _newSourceDialog
        height: Math.min(parent.height * 0.5, 300)
        width: Math.min(parent.width * 0.4, 300)
    }
}
