import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami

Dialog {

    clip: true
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2

    Kirigami.FormLayout {
        anchors.fill: parent

        TextField {
            Kirigami.FormData.label: "Url:"
        }
        Kirigami.Separator {
            Kirigami.FormData.label: "Section Title"
            Kirigami.FormData.isSection: true
        }
        TextField {
            Kirigami.FormData.label: "Label:"
        }
        TextField {
        }
    }
}
